;#lang scheme

(require "graph.ss")
(require "naked.ss")
(require "logger.ss")

(open-log "test-log.html")

(define g (make-graph '() '()))

(set! g (naked-add-node g "one"))
(set! g (naked-add-node g "two"))
(set! g (naked-add-edge g "one" "two" "n"))
(set! g (naked-add-node g "three"))
(set! g (naked-add-edge g "two" "three" "n"))
(set! g (naked-add-node g "four"))
(set! g (naked-add-edge g "four" "three" "n"))
(set! g (naked-add-edge g "one" "four" "n"))

;(display g)(newline)

(set! g (naked-node-describe g "one" "the first place"))
(set! g (naked-node-add-object g "four" "an object"))
(set! g (naked-node-add-object g "four" "another object"))

(save-game-state g "test.graph")

 
