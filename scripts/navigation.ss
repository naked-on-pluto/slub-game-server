;; Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
;;                                       
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#lang scheme
(require "blob.ss" "list.ss" "graph.ss")
(provide (all-defined-out))

(blob nav-node none id edges dlist) 

(define (nav-id n) (car n))
(define (nav-dist n) (cadr n))

(define (ass-set-cons n l)
  (cond 
   ((null? l) (list n))
   ((eq? (caar l) (car n))
    (cons n (ass-set-cons n (cdr l))))
   (else
    (cons (car l) (ass-set-cons n (cdr l))))))

(define (nav-node-walk graph node distance dlist v)
  (foldl
   (lambda (e l)
     (let ((existing (assq (edge-dst e) l)))
       (if (and ;(not (eq? (edge-dst e) (node-id node))) 
                (or (not existing)
                    (> (nav-dist existing) distance)))
           (nav-node-walk graph (graph-get graph (edge-dst e)) (+ distance 1) 
                          (ass-set-cons (list (node-id node) distance) l) v)
           (ass-set-cons (list (node-id node) distance) l))))
   dlist
   (node-edges node)))

(define (build-nav-node graph node)
  (make-nav-node
   (node-id node)
   (map
    (lambda (edge)
      (edge-dst edge))
    (node-edges node))
   (nav-node-walk graph node 0 '() 0)))
     
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(blob nav none nodes)

(define (build-nav graph)
  (make-nav
   (map
    (lambda (node)
      (build-nav-node graph node))
    (graph-nodes graph))))

(define (find-node nav id)
  (foldl
   (lambda (node r)
     (if (and (not r) (eq? (nav-node-id node) id))
         node r))
   #f
   (nav-nodes nav)))

(define (nav-connected? nav loc dst)
  (assq dst (nav-node-dlist (find-node nav loc))))

(define (nav->dot nav graph dst)
  (apply string-append
         (append
          (list "digraph {\n")
          (map 
           (lambda (nn)
             (string-append 
              (number->string (nav-node-id nn)) " [label=\""
              (number->string (nav-node-id nn)) " : "
              (let ((d (assq dst (nav-node-dlist nn))))
                (if d
                    (number->string (cadr d))
                    "unreachable"))
                "\"]\n"))
           (nav-nodes nav))
          (map
           (lambda (ee)
             (string-append
              (number->string (car ee))
              "->"
              (number->string (cadr ee)) "\n"))
           (graph-edges graph))
          (list "}"))))

(define (nav->dot2 nav graph src dst loc)
  (apply string-append
         (append
          (list "digraph {\n")
          (map 
           (lambda (nn)
             (string-append 
              (number->string (nav-node-id nn)) " [label=\""
              (let ((d (assq dst (nav-node-dlist nn))))
                (if d
                    (number->string (cadr d))
                    "unreachable"))             
              "\",style=filled,color="
              (cond 
               ((eq? (nav-node-id nn) loc) "blue")
               ((eq? (nav-node-id nn) src) "red")
               ((eq? (nav-node-id nn) dst) "green")
               (else "gray"))
              "]\n"))
           (nav-nodes nav))
          (map
           (lambda (ee)
             (string-append
              (number->string (car ee))
              "->"
              (number->string (cadr ee)) "\n"))
           (graph-edges graph))
          (list "}"))))
           

; returns a list containing the id of the next node to go to
; and the distance to the destination remaining. returns -1 if we
; are stuck (no exits from this node)
(define (navigate nav loc-id dest-id)
  (if (eq? loc-id dest-id) 
      (list -1 0)
      (let ((node (find-node nav loc-id)))
        (if (not node)
            (list -1 -1)
            (foldl
             (lambda (neighbor-id r)
               ;(printf "search: ~a ~a" neighbor-id (nave
               (let* ((neighbor (find-node nav neighbor-id))
                      (n (assq dest-id (nav-node-dlist neighbor))))
                 (if (and n (< (nav-dist n) (nav-dist r))) 
                     (list neighbor-id (nav-dist n)) r)))
               (list -1 999999)
             (nav-node-edges node))))))

(define (save-navdot nav graph src dst pos filename)
  (let ((f (open-output-file filename #:exists 'replace)))
    (display (nav->dot2 nav graph src dst pos) f)
    (close-output-port f)))
   
 (define (string-pad b)
   (substring (number->string (+ b 100000)) 1 6))

(define (unit-test-nav)
  (define (loop nav graph start pos dst frame)
    ;(printf "~a ~a ~n" pos dst)
    (let ((n (navigate nav pos dst)))
    ;  (display (cadr n))(newline)
      (cond 
       ((> (car n) 0)
        (save-navdot nav graph start dst (car n) (string-append "dot/frame-" (string-pad frame) ".dot"))
        (loop nav graph start (car n) dst (+ frame 1)))
       (else frame))))
  (define (test frame)
    (define g (make-random-graph 50 100))
    #;(define g 
      (let* ((f (open-input-file "game.graph"))
             (l (cadr (read f))))
        (close-input-port f)
        l))
    #;(define g (make-graph
               (list
                (make-node 0 (list (make-edge 1)))
                (make-node 1 (list (make-edge 0) (make-edge 3) (make-edge 2)))
                (make-node 2 '())
                (make-node 3 (list (make-edge 4) (make-edge 1)))
                (make-node 4 (list (make-edge 5)))
                (make-node 5 (list (make-edge 6)))
                (make-node 6 (list (make-edge 3)))
                (make-node 7 (list (make-edge 4)))
                ;(make-node 8 (list (make-edge 9) (make-edge 3)))
                ;(make-node 9 (list (make-edge 10) (make-edge 0)))
                ;(make-node 10 (list (make-edge 7) (make-edge 4) (make-edge 5) (make-edge 0)))
                )))
    (define n (build-nav g))
    (display (nav->dot n g 1))(newline)
    ;(display n)(newline)
    (for ((i (in-range 0 20)))
         (let* ((pos (node-id (choose (graph-nodes g))))
                (dst (node-id (choose (graph-nodes g)))))
                 (display "-----------------------")(newline)
           (when (nav-connected? n pos dst)
                  (display "doo")(newline)
                 (set! frame (loop n g pos pos dst frame)))))
    frame)
  (let ((frame 0))
    (for ((i (in-range 0 30)))
         (display "LOOP")(newline)
         (set! frame (test frame)))))

(display "unit testing navigation.ss")(newline)
;(unit-test-nav)
