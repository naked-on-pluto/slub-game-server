#!/usr//bin/env mzscheme
#lang scheme/base
;; Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
;;                                        
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require scheme/system
         scheme/foreign
         scheme/cmdline
         web-server/servlet
         web-server/servlet-env
         xml
         "scripts/server-interface.ss"
         "scripts/request.ss"
         "scripts/logger.ss")

; a utility to change the process owner,
; assuming mzscheme is called by root.
(unsafe!)
(define setuid (get-ffi-obj 'setuid #f (_fun _int -> _int)))

(define registered-requests
  (list
   (register (req 'login '(id name gender)) login)
   (register (req 'remove-player '(uid id)) remove-player)
   (register (req 'get-node '(uid name)) get-node)
   (register (req 'talk-bot '(uid entity-id pos)) talk-bot)
   (register (req 'pickup '(uid id)) pickup)
   (register (req 'drop '(uid id)) drop)
   (register (req 'like '(uid id)) like)
   (register (req 'inventory '(uid)) inventory)   
   (register (req 'say '(uid id msg)) say)
   (register (req 'add-node '(uid name)) add-node)
   (register (req 'describe-node '(uid info)) describe-node)
   (register (req 'rename-object '(uid id name)) rename-object)
   (register (req 'home '(uid id)) home)
   (register (req 'dress '(uid id garment-id)) dress)
   (register (req 'rename-node '(uid name)) rename-node)
   (register (req 'nuke-node '(uid)) nuke-node)
   (register (req 'add-vocab '(uid vocab)) add-vocab)
   (register (req 'add-entity '(uid id name desc)) add-entity)
   (register (req 'add-bot '(uid id name desc)) add-bot)
   (register (req 'program-bot '(uid id action)) program-bot)
   (register (req 'describe-entity '(uid entity-id desc)) describe-entity)
   (register (req 'lock '(uid name lock-type)) lock)
   (register (req 'remove-entity '(uid entity-id)) remove-entity)
   (register (req 'vote-for '(uid)) vote-for)
   (register (req 'vote-against '(uid)) vote-against)
   (register (req 'get-votes '()) get-votes)   
   (register (req 'save '()) save-state)
   (register (req 'get-node-library '(name)) get-node-library)))

(define (start request)
  (init)
  (let ((values (url-query (request-uri request))))
;    (log (format "~a" values))
    (if (not (null? values)) ; do we have some parameters?      
        (let ((name (assq 'function_name values)))
          (when name ; is this a well formed request?
                (request-dispatch
                 registered-requests             
                 (req (string->symbol (cdr name))
                      (filter
                       (lambda (v)
                         (not (eq? (car v) 'function_name)))
                       values)))))
        "hello")))

(printf "server is running...~n")

; Here we become the user 'nobody'.
; This is a security rule that *only works* if nobody owns no other processes
; than mzscheme. Otherwise better create another dedicated unprivileged user.
; Note: 'nobody' must own the state directory and its files.

;(setuid 65534)

;;

(serve/servlet start
               ; port number is read from command line as argument
               ; ie: ./server.scm 8080	
               #:port (string->number (command-line #:args (port) port))
               #:command-line? #t
               #:servlet-path "/main"
               #:server-root-path
               (build-path "../slub-game-client/"))

